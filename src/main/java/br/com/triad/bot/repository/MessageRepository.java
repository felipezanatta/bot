package br.com.triad.bot.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import br.com.triad.bot.domain.Message;

public interface MessageRepository extends MongoRepository<Message, String> {
	
	@Query(value="{ 'conversationId' : ?0 }",fields="{ 'id' : 1, 'conversationId' : 1, 'timestamp' : 1, 'from' : 1, 'to' : 1, 'text' : 1}")
	public List<Message> findByTheMessagesConversationId(String conversationId);

}
