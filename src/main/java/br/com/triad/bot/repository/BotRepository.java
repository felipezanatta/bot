package br.com.triad.bot.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.com.triad.bot.domain.Bot;

public interface BotRepository extends MongoRepository<Bot, String> {

}
