package br.com.triad.bot.controller;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.triad.bot.domain.Bot;
import br.com.triad.bot.repository.BotRepository;

@RestController
@RequestMapping("/bots")
public class BotAPI {

	@Autowired
	private BotRepository botRepo;

	private final Logger log = LoggerFactory.getLogger(BotAPI.class);

	@GetMapping
	public Iterable<Bot> getAll() {
		return botRepo.findAll();
	}

	@GetMapping("/bots/{id}")
	public ResponseEntity<?> getById(@PathVariable("id") String id) {
		log.info("Searching by id " + id);
		Optional<Bot> optional = botRepo.findById(id);
		if (optional.isPresent()) {
			return new ResponseEntity<Bot>(optional.get(), HttpStatus.OK);

		} else {
			log.warn("ID NOT FOUND" + id);
			return new ResponseEntity<String>("Id not found", HttpStatus.BAD_REQUEST);
		}
	}

	@PostMapping(consumes = "application/json", produces = "application/json")
	public ResponseEntity<?> insert(@RequestBody Bot bot) {
		if (bot != null && !"".equals(bot.getId()) && bot.getId() != null && !"".equals(bot.getName())
				&& bot.getName() != null) {
			Bot insertId = botRepo.insert(bot);
			log.info("BOT INSERTED - ID:" + bot.getId());
			return new ResponseEntity<Bot>(insertId, HttpStatus.CREATED);
		} else {
			String messageError = "Validation Error no BOT.";
			log.warn(messageError);
			return new ResponseEntity<String>(messageError, HttpStatus.CREATED);
		}

	}

	@PutMapping(path = "/bots/{id}", consumes = "application/json", produces = "application/json")
	public ResponseEntity<?> update(@PathVariable("id") String id, @RequestBody Bot requestBot) {

		log.info("BOT UPDATE - ID:" + id);

		Optional<Bot> botRegOpt = botRepo.findById(id);

		if (botRegOpt.isPresent()) {
			Bot botRep = botRegOpt.get();
			if (!"".equals(requestBot.getName()) && requestBot.getName() != null) {
				botRep.setName(requestBot.getName());
				botRepo.save(botRep);
				log.info("BOT UPDATED - ID:" + id);
				return new ResponseEntity<Bot>(HttpStatus.OK);
			} else {
				log.error("BOT NOT UPDATED - ID:" + id + " Name not valid");
				return new ResponseEntity<String>("Name not valid", HttpStatus.BAD_REQUEST);
			}

		} else {
			String msg = "Id not found";
			log.error("BOT NOT UPDATED - ID:" + id + " " + msg);
			return new ResponseEntity<String>(msg, HttpStatus.BAD_REQUEST);
		}
	}

	@DeleteMapping("/bots/{id}")
	public ResponseEntity<?> delete(@PathVariable("id") String id) {
		Optional<Bot> optional = botRepo.findById(id);
		if (optional.isPresent()) {
			botRepo.deleteById(id);
			return new ResponseEntity<>(HttpStatus.OK);

		} else {
			String msg = "ID NOT FOUND:" + id;
			log.error(msg);
			return new ResponseEntity<String>(msg, HttpStatus.BAD_REQUEST);
		}
	}
}
