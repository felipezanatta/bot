package br.com.triad.bot.controller;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.triad.bot.domain.Message;
import br.com.triad.bot.repository.MessageRepository;

@RestController
public class MessageAPI {

	@Autowired
	private MessageRepository msgRepository;

	private final Logger log = LoggerFactory.getLogger(MessageAPI.class);

	@GetMapping
	public Iterable<Message> getAll() {
		return msgRepository.findAll();
	}

	@GetMapping("/messages/{id}")
	public ResponseEntity<?> getById(@PathVariable("id") String id) {
		log.info("Searching by id " + id);
		Optional<Message> optional = msgRepository.findById(id);
		if (optional.isPresent()) {
			return new ResponseEntity<Message>(optional.get(), HttpStatus.OK);

		} else {
			log.warn("ID NOT FOUND" + id);
			return new ResponseEntity<String>("Id not found", HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("/messages")
	public Iterable<Message> getByConversationId(
			@RequestParam(name = "conversationId", required = true) String conversationId) {
		log.info("Searching by conversationId " + conversationId);
		return msgRepository.findByTheMessagesConversationId(conversationId);
	}

	@PostMapping(path = "/messages", consumes = "application/json", produces = "application/json")
	public ResponseEntity<?> insert(@RequestBody Message message) {
		if (message != null && !"".equals(message.getConversationId()) && message.getConversationId() != null
				&& !"".equals(message.getTimestamp()) && message.getTimestamp() != null && !"".equals(message.getFrom())
				&& message.getFrom() != null && !"".equals(message.getTo()) && message.getTo() != null
				&& !"".equals(message.getText()) && message.getText() != null) {
			Message insertMessage = msgRepository.insert(message);
			log.info("MESSAGE INSERTED - Message :" + message.getText());
			return new ResponseEntity<Message>(insertMessage, HttpStatus.CREATED);
		} else {
			String messageError = "Validation Error no BOT.";
			log.warn(messageError);
			return new ResponseEntity<String>(messageError, HttpStatus.CREATED);
		}

	}
}
