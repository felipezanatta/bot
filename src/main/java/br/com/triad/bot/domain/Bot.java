package br.com.triad.bot.domain;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "bot")
public class Bot {

	private String id;
	private String name;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Bot{" + ", id='" + id + '\'' + ", name=" + name + '}';
	}

}
