package br.com.triad.bot.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Document(collection = "message")
public class Message {

	@Id
	private String id;

	private String conversationId;

	private String timestamp;

	private String from;

	private String to;

	private String text;

	
	
	public String getId() {
		return id;

	}

	public void setId(String id) {
		this.id = id;
	}

	public String getConversationId() {
		return conversationId;
	}

	public void setConversationId(String conversationId) {
		this.conversationId = conversationId;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return "Message{" + ", conversationId = " + conversationId + '\'' + ", timestamp = " + timestamp + '\''
				+ ", from = " + from + '\'' + ", to = " + to + '\'' + ", text = " + text + '\'' + '}';
	}
}
